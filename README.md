Das hier verfügbare Shell-Script legt auf einem Debian Bullseye (noch testing) einen CryptPad-Server an.

# Minimale Anforderung

Entsprechend der [Installation für einen Produktions-Server](https://docs.cryptpad.fr/en/admin_guide/installation.html) sind die folgenden Anforderungen zu erfüllen.

## Hardware
- Debian 10
- 2GB RAM
- 2 CPUs
- 20GB Speicherplatz

## Domains und SSL-/TLS-Zertifikate
- Es werden mind. zwei, besser vier (Sub-)Domain-Namen benötigt
- **Ein** Zertifikat das **beide Namen** abdeckt

In dieser Test-Installation werden für die beiden **unbedingt** notwendigen Domain-Namen folgende Angaben verwendet:
- `cryptpad.home.grupp-web.de` (*Hauptserver der von Außen angesprochen wird*)
- `sandbox.home-grupp-web.de`

Für Websockets und verschlüsselte BLOBs (Binary Large Objects) kann CryptPad diese über eigene Server bereitstellen. Kann zur Leistungsverteilung bei größere Anzahl an Usern verwendet werden / notwendig werden. Für kleine/normale Installation kann das über den Hauptserver abgedeckt werden. In dieser Test-Installation wird deshalb in der Nginx-Konfiguration für die Variablen `$api_domain` und `$files_domain` jeweils der Hauptserver `cryptpad.home.grupp-web.de` hinterlegt.

Das Skript verwendet die beiden Hostnamen `cryptpad` und `sandbox` fest kodiert. Der Domain-Bereich (BASEDOMAIN), hier `home.grupp-web.de`, wird aber im Skriptverlauf abgefragt.

# Installation (aus dem Source-Code)

Es gibt zwar die Möglichkeit per Docker zu installieren, was aber ein Community-Projekt ist und nicht offiziell unterstützt wird. Das hier verfügbare Skript hält sich sehr eng an die oben verlinkte Installationsanleitung. Abweichungen sind:
- Statt eines per `nvm` installierten `node.js`, in einer älteren Version, wird das vom Debian-Projekt gepflegte `nodejs`-Paket verwendet. Das ist bei Debian 11 nach wie vor noch eine Version 12.x, aber es handelt sich um eine neure Version die über Systemupdates gepflegt wird.
- Das `systemd`-Servicefile musste entsprechend auf den `node`-Aufruf angepasst werden.
- Statt echter SSL-/TLS-Zertifikate wird das `snakeoil`-Testzertifikat aus dem `ssl-cert`-Paket verwendet.

Alternativ findet man zur Installation eines CryptPad-Servers auch Ansible-Playbooks im Netz.

Zur Installation das Skript als User `root` herunterladen, ausführbar machen und starten:

```
wget https://codeberg.org/angry/CryptPad-Shellinstaller/raw/branch/master/install.sh
chmod u+x install.sh
./install.sh
```

## CryptPad Admin-Account erstellen

- Wenn wie hier mit einem selbstsignierten SSL-/TLS-Zertifikat gerabeitet wird, muss im Browser zuerst der `sandbox`-Server einmal aufgerufen werden. Eine Ausnahmeregel für das Zertifikat hinzufügen. Erst anschließend den `cryptpad`-Server selbst aufrufen - auch dort eine Ausnahmeregel für das Zertifikat hinzufügen.
- Auf der CryptPad-Startseite, über die Schaltfläche "**Registrieren**", einen Benutzer:innen-Account anlegen.
- **User-Icon (oben, rechts)** anklicken und "**Einstellungen**" auswählen
- Den kompletten Text im Feld "**Öffentlicher Schlüssel zum Unterschreiben**" in die Zwischenablage kopieren
- Auf dem Server die Datei `/home/cryptpad/cryptpad/config/config.js` editieren. Im Abschnitt "Admin" den 3-zeiligen `adminKeys`-Block aktivieren, und den öffentlichen Schlüssel aus der Zwischenablage in diesen Block als aktive Zeile einfügen.
- **ACHTUNG:** Beachten, dass um die eckigen Klammern noch doppelte Anführungszeichen platziert sein müssen, und am Ende der Zeile ein Komma sein muss!
- Speichern und Editor verlassen!
- Mit `systemctl restart cryptpad`

Wenn die Seite anschließend neu geladen wurde, ist im User-Menü nun der Menüpunkt "**Administration**" verfügbar.

# To-Do für Produktions-Server

## DH-Parameter neu erzeugen

Grundsätzlich den DH-Parameter aus dem Installationspaket mit eigenem DH-Parameter ersetzen. Dieses Skript hat nur einen 1024-Bit-Parameter erstellt. Für einen Produktions-Server sollte hier ein 4096-Bit-Paramter angelegt werden.  **ACHTUNG:** Das ist rechenintensiv und dauert auch auf leistungsfähigen Prozessoren eine Weile. Ggf. die Datei erst mal an anderer Stelle erzeugen und dann an die richtige Position schieben.

`openssl dhparam -out /etc/nginx/dhparam.pem 4096`

## Echtes SSL-/TLS-Zertifikat installieren

Für einen Produktionsserver wird natürlich noch ein SSL-/TLS-Zertifikat benötigt das von den gängigen Browsern auch ohne Ausnahmegenehmigungen akzeptiert wird. Bekannteste Variante ist hier *Lets Encrypt* als Zertifikatsaussteller. Beachten Sie, dass das Zertifikat für beide Hostnamen gültig sein muss. Vorgeschlagen ist, dass der Hauptname auf `cryptpad.<BASEDOMAIN>` läuft, und ein sogenannter Alternativ-Namen auf `sandbox.<BASEDOMAIN>` enthalten ist. Das Zertifikat entsprechend in der Nginx-Konfiguration unter `/etc/nginx/sites-available/cryptpad.conf` passend einkonfigurieren.
