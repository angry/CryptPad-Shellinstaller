#!/bin/bash
#
# CryptPad installation (with Snakeoil-TLS-certificate) on
# Debian Bullseye (testing)
#
# by Andreas Grupp, v0.1.1

# Notwendige Pakete als root installieren
# Installation nimmt node.js per nvm in Version 12.14.0
# Debian 11 bringt ein sicheres node.js Version 12.21.0!
# Bevorzugt, da auch Updates automatisch geliefert werden
apt install -y curl git nginx-full nodejs npm ssl-cert

# Bower (Paketmanager für clientseitige Webentwicklung) installieren
npm install -g bower

# Eigenen User für CryptPad anlegen (empfohlen)
useradd -m -c CryptPad-User -s /bin/bash cryptpad

# Zertifikate für SSH-Login installieren, hier einfach
# durch Kopie der betreffenden Datei von root
mkdir ~cryptpad/.ssh
cp ~/.ssh/authorized_keys ~cryptpad/.ssh
chown -R cryptpad:cryptpad ~cryptpad/.ssh

# Installation von CryptPad
cd /home/cryptpad
git clone https://github.com/xwiki-labs/cryptpad.git cryptpad
cd /home/cryptpad/cryptpad
npm install
npm audit fix --force
bower install

# Konfiguration auf Basis der Beispiel-Konfiguration übernehmen
cd /home/cryptpad/cryptpad/config
cp config.example.js config.js

# Alle Dateien dem User 'cryptpad' übereignen
cd /home/cryptpad
chown -R cryptpad:cryptpad cryptpad

# Systemd Service-File installieren - wieder als root, und Manipulation
# des Original-systemd-Service-Files um node.js von Debian zu verwenden
cp ~cryptpad/cryptpad/docs/cryptpad.service /etc/systemd/system
sed -i 's/^ExecStart.*$/ExecStart=\/usr\/bin\/node \/home\/cryptpad\/cryptpad\/server.js/' /etc/systemd/system/cryptpad.service

# Nginx konfigurieren
openssl dhparam -out /etc/nginx/dhparam.pem 1024
cd /etc/nginx/sites-available
cp ~cryptpad/cryptpad/docs/example.nginx.conf ./cryptpad.conf
echo -e "\n\nEs werden 2 Server-Namen erzeugt. Bitte Basis-Domain dafuer"
echo -e "eingeben. Als Namen werden 'cryptpad' und 'sandbox' verwendet."
echo -e "Beispiel: Bei Eingabe von 'schule.de' werden die beiden Namen"
echo -e "'cryptpad.schule.de' und 'sandbox.schule.de' verwendet.\n"
echo -n "Bitte Basis-Domain eingeben -> "
read BASEDOMAIN
sed -i "s/set \$main_domain.*$/set \$main_domain \"cryptpad.${BASEDOMAIN}\";/" cryptpad.conf
sed -i "s/set \$sandbox_domain.*$/set \$sandbox_domain \"sandbox.${BASEDOMAIN}\";/" cryptpad.conf
sed -i "s/set \$api_domain.*$/set \$api_domain \"cryptpad.${BASEDOMAIN}\";/" cryptpad.conf
sed -i "s/set \$files_domain.*$/set \$files_domain \"cryptpad.${BASEDOMAIN}\";/" cryptpad.conf
sed -i "s/server_name.*;$/server_name cryptpad.${BASEDOMAIN} sandbox.${BASEDOMAIN};/" cryptpad.conf
sed -i "s/^    ssl_certificate    .*;$/    ssl_certificate         \/etc\/ssl\/certs\/ssl-cert-snakeoil.pem;/" cryptpad.conf
sed -i "s/^    ssl_certificate_key.*;$/    ssl_certificate_key     \/etc\/ssl\/private\/ssl-cert-snakeoil.key;/" cryptpad.conf
sed -i "s/^    ssl_trusted_certificate/    # ssl_trusted_certificate/" cryptpad.conf

# Server-Namen auch in der CryptPad-Konfiguration anpassen
cd ~cryptpad/cryptpad/config/
sed -i "s/httpUnsafeOrigin.*,$/httpUnsafeOrigin: 'https:\/\/cryptpad.${BASEDOMAIN}',/" config.js
sed -i "s/\/\/ httpSafeOrigin.*,$/httpSafeOrigin: 'https:\/\/sandbox.${BASEDOMAIN}',/" config.js

# Generelles Enabling und anschließender Start von CryptPad
systemctl enable --now cryptpad

# CryptPad-Website aktivieren und Nginx neu starten
cd /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available/cryptpad.conf
systemctl restart nginx

# Abschlussmeldung
echo -e "\n\nInstallation abgeschlossen! Ihre CryptPad-Instanz sollte unter dem URL"
echo "https://cryptpad.${BASEDOMAIN} verfügbar sein. Sofern dieser Hostnamen, sowie"
echo "der Hostnamen https://sandbox.${BASEDOMAIN} nicht über DNS aufgelöst werden,"
echo "fügen Sie bitte in der /etc/hosts-Datei Ihres Clients eine passend Zeile ein."
echo -e "Beispiel:\n\n172.16.0.214  cryptpad.${BASEDOMAIN} sandbox.${BASEDOMAIN}\n\n"

